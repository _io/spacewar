﻿using UnityEngine;

public class OnTrailDestroy : MonoBehaviour
{
    // Inspector variables
    [SerializeField] private TrailRenderer trailRenderer;

    // When object is destroyed
    private void OnDestroy()
    {
        // Remove trail from list
        GameManager.Instance.TrailRenderers.Remove(trailRenderer);
    }
}
