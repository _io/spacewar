﻿using System.Collections;
using UnityEngine;
using Mirror;

public class BulletBehaviour : NetworkBehaviour
{
    // Inspector variables
    [SerializeField] private bool autoDestruct = true;
    [SerializeField] private float activationDelay = 0.5f;
    [SerializeField] private float delayToDisappear = 4f;
    [SerializeField] private float delayToDestroy = 8f;
    [SerializeField] private GameObject explosionPrefab;
    [SerializeField] private GameObject spriteLightObject;
    [SerializeField] private Rigidbody2D rigidBody;
    [SerializeField] private Collider2D collider;
    [SerializeField] private SpriteRenderer renderer;

    // Private variables
    private float timeWaited = 0f;
    private float delayTimeWaited = 0f;

    // Init
    private void Start()
    {
        // Add to list of rigidbodies
        GameManager.Instance.RigidBodies.Add(rigidBody);

        // Countdown lifetime
        StartCoroutine(ActivateAfterDelay());
        if (autoDestruct)
        {
            if (!GameManager.Instance.NetManager.isNetworkActive || isServer)
                StartCoroutine(DestroyAfterDelay(delayToDisappear));
        }
    }

    // Activate collider after delay
    IEnumerator ActivateAfterDelay()
    {
        // Make object disappear
        while (delayTimeWaited < activationDelay)
        {
            if (!GameManager.Instance.IsPaused)
                delayTimeWaited += Time.deltaTime;

            yield return null;
        }
        collider.enabled = true;
    }

    // Check for collisions
    public void OnTriggerEnter2D(Collider2D collision)
    {
        // Handle collisions on server side
        if (GameManager.Instance.NetManager.isNetworkActive && !isServer) return;

        StopAllCoroutines();
        StartCoroutine(DestroyAfterDelay(0f));
        
        ShipCollision shipCollision = collision.gameObject.GetComponent<ShipCollision>();
        if (shipCollision != null)
        {
            shipCollision.DestroyShip();
        }
    }

    // Hide visual/gameplay elements
    private void MakeDisappear()
    {
        collider.enabled = false;
        spriteLightObject.SetActive(false);
        if (rigidBody != null) rigidBody.bodyType = RigidbodyType2D.Static;
        if (renderer != null) renderer.enabled = false;
    }

    // Destroy after n seconds
    IEnumerator DestroyAfterDelay(float disappearDelay)
    {
        // Make object disappear
        while (timeWaited < disappearDelay)
        {
            if (!GameManager.Instance.IsPaused)
                timeWaited += Time.deltaTime;

            yield return null;
        }
        GameManager.Instance.RigidBodies.Remove(rigidBody);

        MakeDisappear();
        if (GameManager.Instance.NetManager.isNetworkActive) RpcHideBullet();

        GameObject explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
        if (GameManager.Instance.NetManager.isNetworkActive)
            NetworkServer.Spawn(explosion);

        // Actually destroy game object
        while (timeWaited < delayToDestroy)
        {
            if (!GameManager.Instance.IsPaused)
                timeWaited += Time.deltaTime;

            yield return null;
        }

        // Destroy object
        if (GameManager.Instance.NetManager.isNetworkActive)
            NetworkServer.Destroy(gameObject);
        else
            Destroy(this.gameObject);
    }

    // Hide bullet on client side
    [ClientRpc]
    void RpcHideBullet() => MakeDisappear();
}
