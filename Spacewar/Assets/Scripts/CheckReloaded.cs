﻿using System.Collections;
using UnityEngine;

public class CheckReloaded : MonoBehaviour
{
    // Inspector variables
    [SerializeField] private float delayBetweenShots = 2f;

    // Public variables
    [System.NonSerialized] public bool CanShoot = true;

    // Private variables
    private float timeWaited = 0f;

    // Call reload externally
    public void Reload()
    {
        if (CanShoot == true)
            StartCoroutine(ShotFired());
    }

    // Start reload timer
    IEnumerator ShotFired()
    {
        CanShoot = false;
        while (timeWaited < delayBetweenShots)
        {
            if (!GameManager.Instance.IsPaused)
                timeWaited += Time.deltaTime;
            yield return null;
        }
        CanShoot = true;
        timeWaited = 0f;
    }
}
