﻿using System.Collections;
using UnityEngine;

public class AI : MonoBehaviour
{
    // Inspector variables
    [SerializeField] private Rigidbody2D rigidBody;
    [SerializeField] private float acceleration = 0.05f;
    [SerializeField] private float obstacleAvoidanceFactor = 0.25f;
    [SerializeField] private float maxSpeed = 0.5f;
    [SerializeField] private float fireRatePercentage = 50f;
    [SerializeField] private float bulletForce = 1f;
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform gunPosition;
    [SerializeField] private Sprite staticFrame;
    [SerializeField] private Sprite thrustFrame;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private CheckReloaded checkReloadedScript;

    // Private variables
    private Transform obstacle;
    private Transform target;

    // Init
    private void Start()
    {
        // Setup player
        GameManager.Instance.RigidBodies.Add(rigidBody);
        obstacle = GameObject.Find("Sun").transform;
        target = GameObject.Find("Prefab_Player(Clone)").transform;
        StartCoroutine(TryFire());
    }

    // Update is called once per frame
    void Update()
    {
        // Game paused
        if (GameManager.Instance.IsPaused) { return; }

        // AI Movement
        float sunDX = (gameObject.transform.position.x - obstacle.position.x);
        float sunDY = (gameObject.transform.position.y - obstacle.position.y);
        float towardPlayerX = (target.position.x - gameObject.transform.position.x);
        float towardPlayerY = (target.position.y - gameObject.transform.position.y);
        float horizontalThrust = Mathf.Lerp(sunDX, towardPlayerX, Mathf.Abs(sunDX) * obstacleAvoidanceFactor);
        float verticalThrust = Mathf.Lerp(sunDY, towardPlayerY, Mathf.Abs(sunDY) * obstacleAvoidanceFactor);
        Vector2 thrustVector = (new Vector2(horizontalThrust, verticalThrust).normalized * (acceleration * GameManager.Instance.CurrentGameSpeed)) * Time.deltaTime;

        // Apply movement
        rigidBody.AddForce(thrustVector);

        // Cap speed
        float currentSpeed = rigidBody.velocity.magnitude;
        if (currentSpeed > (maxSpeed * GameManager.Instance.CurrentGameSpeed))
            rigidBody.velocity = rigidBody.velocity.normalized * (maxSpeed * GameManager.Instance.CurrentGameSpeed);

        // Rotation
        float angle = -(Mathf.Atan2(horizontalThrust, verticalThrust)) * Mathf.Rad2Deg;
        if (Mathf.Abs(angle) > 0)
        {
            spriteRenderer.sprite = thrustFrame;
            gameObject.transform.rotation = Quaternion.Euler(0, 0, angle);
        }
        else
            spriteRenderer.sprite = staticFrame;
    }

    // Fire at random intervals
    IEnumerator TryFire()
    {
        yield return new WaitForSeconds(1f);
        while (isActiveAndEnabled)
        {
            if (!GameManager.Instance.IsPaused)
            {
                int randomPercentage = Random.Range(0, 100);
                if (randomPercentage < fireRatePercentage)
                    CreateProjectile();
            }
            yield return new WaitForSeconds(1f);
        }
    }

    // Shoot gun
    public void CreateProjectile()
    {
        // Return if not reloaded
        if (!checkReloadedScript.CanShoot) return;

        GameObject bullet = Instantiate(bulletPrefab, gunPosition.position, gunPosition.rotation);
        Rigidbody2D bulletRB = bullet.GetComponent<Rigidbody2D>();
        bulletRB.AddForce((Vector3)rigidBody.velocity + (gunPosition.up * (bulletForce * GameManager.Instance.CurrentGameSpeed)), ForceMode2D.Impulse);
        checkReloadedScript.Reload();
        ScreenWrap screenWrapComponent = bullet.GetComponent<ScreenWrap>();
        screenWrapComponent.WrapObjectTransform = true;
        screenWrapComponent.WrapTrailRenderer = true;
    }
}
