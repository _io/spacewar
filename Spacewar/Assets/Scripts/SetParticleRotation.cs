﻿using UnityEngine;

public class SetParticleRotation : MonoBehaviour
{
    // Get unity components
    [SerializeField] private ParticleSystem particleSystem;
    [SerializeField] private Rigidbody2D shipRigidBody;

    // Update is called once per frame
    void Update()
    {
        // Set rotation
        particleSystem.startRotation = -shipRigidBody.rotation * Mathf.Deg2Rad;
    }
}
