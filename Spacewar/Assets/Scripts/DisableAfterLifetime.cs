﻿using UnityEngine;
using System.Collections;

public class DisableAfterLifetime : MonoBehaviour
{
    // Inspector variables
    [SerializeField] private bool destroy = true;
    [SerializeField] private float delay = 8f;

    // Private variables
    private float timeWaited = 0f;

    // Init
    private void OnEnable()
    {
        timeWaited = 0f;
        StartCoroutine(DestroyAfterDelay());
    }

    // Destroy after n seconds
    IEnumerator DestroyAfterDelay()
    {
        // Make object disappear
        while (timeWaited < delay)
        {
            if (!GameManager.Instance.IsPaused)
                timeWaited += Time.deltaTime;

            yield return null;
        }
        if (destroy)
            Destroy(this.gameObject);
        else
            gameObject.SetActive(false);
    }
}
