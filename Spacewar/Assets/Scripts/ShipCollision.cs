﻿using UnityEngine;
using Mirror;
using System.Collections;

public class ShipCollision : NetworkBehaviour
{
    // Inspector variables
    [SerializeField] private GameObject explosionPrefab;
    [SerializeField] private Rigidbody2D rigidBody;
    [SerializeField] private NetworkTransform networkTransform;

    // Private variables
    private Vector3 initialPosition;

    // Init
    private void Start()
    {
        // Assign variables
        initialPosition = rigidBody.transform.position;
    }

    // Ship collision with sun
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Only handle collisoin on server side
        if (GameManager.Instance.NetManager.isNetworkActive && !isServer) return;

        if (collision.gameObject.name == "Sun")
            DestroyShip();
        else if (collision.gameObject.tag == "Ship")
        {
            ShipCollision shipCollision = collision.gameObject.GetComponent<ShipCollision>();
            if (shipCollision != null)
            {
                shipCollision.DestroyShip();
            }
        }
    }

    // Destroy ship
    public void DestroyShip()
    {
        // Solo game respawn
        if (!GameManager.Instance.NetManager.isNetworkActive)
        {
            Instantiate(explosionPrefab, transform.position, transform.rotation);
            Respawn();
        }
        else
        {
            // Network explosion
            GameObject obj = Instantiate(explosionPrefab, transform.position, transform.rotation);
            NetworkServer.Spawn(obj);

            // Server ship destroy
            StartCoroutine(RespawnOnNetwork(0.1f));
        }
    }

    // Reset ship
    void Respawn()
    {
        rigidBody.velocity = Vector2.zero;
        rigidBody.angularVelocity = 0f;
        rigidBody.position = initialPosition;
        rigidBody.rotation = 0f;
    }

    // Respawn on client side
    [ClientRpc]
    void RpcRespawn()
    {
        if (!isServer)
            Respawn();
    }

    // Only run on client
    [ClientRpc]
    void RpcSetNetworkTransformState(bool state)
    {
        if (!isServer)
            networkTransform.enabled = state;
    }

    // Allow respawning without interpolation
    IEnumerator RespawnOnNetwork(float seconds)
    {
        // Disable net transforms
        if (isLocalPlayer)
            networkTransform.enabled = false;
        else
            RpcSetNetworkTransformState(false);

        yield return new WaitForSeconds(seconds);

        // Respawn ship
        if (isLocalPlayer)
            Respawn();
        else
            RpcRespawn();

        yield return new WaitForSeconds(seconds);

        // Enable net transforms
        if (isLocalPlayer)
            networkTransform.enabled = true;
        else
            RpcSetNetworkTransformState(true);

        // Respawn ship
        if (isLocalPlayer)
            Respawn();
        else
            RpcRespawn();
    }
}
