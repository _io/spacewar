﻿using System.Collections;
using UnityEngine;
using Mirror;

public class ScreenWrap : NetworkBehaviour
{
    // Inspector variables
    [SerializeField] private bool ServerAuthority = false;
    [SerializeField] public bool WrapObjectTransform = true;
    [SerializeField] public bool WrapTrailRenderer = true;

    // Define variables
    private Rigidbody2D rigidBody;
    private TrailRenderScript trailRenderScript;
    private NetworkTransform networkTransform;
    private bool isWrapping = false;

    // Init
    void Start()
    {
        trailRenderScript = gameObject.GetComponent<TrailRenderScript>();
        networkTransform = gameObject.GetComponent<NetworkTransform>();
        rigidBody = GetComponentInChildren<Rigidbody2D>();
    }

    // Main loop
    void Update()
    {
        if (GameManager.Instance.NetManager.isNetworkActive)
        {
            // Only allow server authority
            if (ServerAuthority && !isServer)
                return;

            // Disable wrapping for non local ships
            if (gameObject.tag == "Ship")
                if (!isLocalPlayer) return;
        }

        // Get current positions
        Vector2 currentScreenPosition = Camera.main.WorldToScreenPoint(rigidBody.position);
        float newPositionX = currentScreenPosition.x;
        float newPositionY = currentScreenPosition.y;

        // Left edge
        if (currentScreenPosition.x < 0)
        {
            if (!isWrapping)
                StartCoroutine(WrapObject(Screen.width, newPositionY));
        }
        // Right edge
        else if (currentScreenPosition.x > Screen.width)
        {
            if (!isWrapping)
                StartCoroutine(WrapObject(0, newPositionY));
        }

        // Bottom edge
        if (currentScreenPosition.y < 0)
        {
            if (!isWrapping)
                StartCoroutine(WrapObject(newPositionX, Screen.height));
        }
        // Top edge
        else if (currentScreenPosition.y > Screen.height)
        {
            if (!isWrapping)
                StartCoroutine(WrapObject(newPositionX, 0));
        }
    }

    // Detach trail, wrap object, and create new trail
    IEnumerator WrapObject(float newX, float newY)
    {
        isWrapping = true;

        // Detach current trail
        if (WrapTrailRenderer)
        {
            // Detach trail
            SetTrail(false);
            if (GameManager.Instance.NetManager.isNetworkActive)
                RpcSetTrail(false);
        }

        // Turn off network transform
        if (GameManager.Instance.NetManager.isNetworkActive)
            networkTransform.enabled = false;

        if (GameManager.Instance.NetManager.isNetworkActive)
            yield return new WaitForSeconds(0.1f);

        // Wrap object
        if (WrapObjectTransform)
        {
            rigidBody.position = Camera.main.ScreenToWorldPoint(new Vector2(newX, newY));
            gameObject.transform.position = rigidBody.position;
        }

        if (GameManager.Instance.NetManager.isNetworkActive)
            yield return new WaitForSeconds(0.1f);

        // Turn back on network transform
        if (GameManager.Instance.NetManager.isNetworkActive)
            networkTransform.enabled = true;

        if (GameManager.Instance.NetManager.isNetworkActive)
            yield return new WaitForSeconds(0.1f);

        // Create new trail
        if (WrapTrailRenderer)
        {
            // Reattach trail
            SetTrail(true);
            if (GameManager.Instance.NetManager.isNetworkActive)
                RpcSetTrail(true);
        }

        isWrapping = false;
        yield return null;
    }

    // Detach or add new trail
    void SetTrail(bool newTrail)
    {
        if (newTrail)
        {
            print("Trail created");
            trailRenderScript.AttachedTrailRenderer = Instantiate(trailRenderScript.TrailRendererPrefab, rigidBody.position, Quaternion.identity, transform);
            TrailRenderer newTrailRenderer = trailRenderScript.AttachedTrailRenderer.GetComponent<TrailRenderer>();
            newTrailRenderer.time = TrailRenderScript.TrailTimeInSeconds * (1f / GameManager.Instance.CurrentGameSpeed);
        }
        else
        {
            print("Trail detached");
            if (trailRenderScript.AttachedTrailRenderer != null)
                trailRenderScript.AttachedTrailRenderer.transform.parent = null;
        }
    }

    // Runs on client
    [ClientRpc]
    void RpcSetTrail(bool newTrail)
    {
        if (!isServer)
            SetTrail(newTrail);
    }
}
