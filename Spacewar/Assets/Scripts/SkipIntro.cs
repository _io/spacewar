﻿using UnityEngine;

public class SkipIntro : MonoBehaviour
{
    // Inspector variables
    public Animator[] AnimationsToSkip;

    // Skip animations
    public void SkipIntroPressed()
    {
        foreach (Animator anim in AnimationsToSkip)
            anim.SetBool("Skip", true);
        gameObject.SetActive(false);
    }
}
