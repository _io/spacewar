﻿using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class PlayerInput : NetworkBehaviour
{
    // Inspector variables
    [SerializeField] private float acceleration = 10f;
    [SerializeField] private float rotationSpeed = 10f;
    [SerializeField] private float maxSpeed = 10f;
    [SerializeField] private float bulletForce = 5f;
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform gunPosition;
    [SerializeField] private Sprite staticFrame;
    [SerializeField] private Sprite thrustFrame;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Rigidbody2D rigidBody;
    [SerializeField] private AudioSource thrustSound;
    [SerializeField] private CheckReloaded checkReloadedScript;

    // Network variables
    [SyncVar(hook = "OnGameSpeedChanged")] private int GameSpeedIndex;
    [SyncVar(hook = "OnPlayerThrustChange")] private bool IsPlayerThrusting;

    // Private variables
    private Joystick joystick;
    private Button fireButton;

    // Network update opponent values
    void OnPlayerThrustChange(bool oldValue, bool isThrusting)
    {
        if (!isLocalPlayer)
        {
            if (isThrusting)
            {
                thrustSound.Play();
                spriteRenderer.sprite = thrustFrame;
            }
            else
            {
                thrustSound.Stop();
                spriteRenderer.sprite = staticFrame;
            }
        }
    }

    // Network changed game speed
    void OnGameSpeedChanged(int oldValue, int speedIndex)
    {
        if (!isClient) return;
        GameManager.Instance.GameSpeedIndex = speedIndex - 1;
        GameManager.Instance.ToggleSpeed(true);
    }

    // Init
    private void Start()
    {
        // Add ship to list
        GameManager.Instance.RigidBodies.Add(rigidBody);
        GameManager.Instance.PlayerRigidBodies.Add(rigidBody);

        // Add input listeners
        joystick = GameManager.Instance.Joystick;
        fireButton = GameManager.Instance.FireButton;
        fireButton.onClick.AddListener(TryShoot);
    }

    // Remove button listeners
    private void OnDestroy()
    {
        fireButton.onClick.RemoveAllListeners();
    }

    // Main loop
    void Update()
    {
        // Network update game speed
        if (GameManager.Instance.NetManager.isNetworkActive)
        {
            GameSpeedIndex = GameManager.Instance.GameSpeedIndex;
            GameManager.Instance.CurrentGameSpeed = GameManager.Instance.GameSpeeds[GameSpeedIndex];
            if (!isLocalPlayer) return;
        }

        // Game paused
        if (GameManager.Instance.IsPaused) { return; }

        Vector2 thrustVector = Vector2.zero;
        float angle = gameObject.transform.rotation.eulerAngles.z;
        if (GameManager.Instance.RetroControlsEnabled || GameManager.Instance.webGLInput)
        {
            // WebGL movement
            if (GameManager.Instance.webGLInput)
            {
                // WebGL retro control thrust
                float thrust = Input.GetKey(KeyCode.UpArrow) ? 1f : 0f;
                thrustVector = ((transform.up * thrust) * (acceleration * GameManager.Instance.CurrentGameSpeed)) * Time.deltaTime;

                // WebGL retro control rotate
                float deltaAngle = Input.GetKey(KeyCode.LeftArrow) ? 1f : 0f;
                if (Input.GetKey(KeyCode.RightArrow)) deltaAngle = -1f;
                angle += deltaAngle * ((rotationSpeed) * Time.deltaTime);
            }
            else
            {
                // Retro control thrust
                float thrust = joystick.Vertical;
                if (thrust < GameManager.Instance.JoystickDeadZone) thrust = 0f;
                thrustVector = ((transform.up * thrust) * (acceleration * GameManager.Instance.CurrentGameSpeed)) * Time.deltaTime;

                // Retro control rotate
                if (thrust <= GameManager.Instance.JoystickDeadZone)
                {
                    float deltaAngle = -joystick.Horizontal * ((rotationSpeed) * Time.deltaTime);
                    angle += deltaAngle;
                }
            }

            // WebGL shoot
            if (Input.GetKeyDown(KeyCode.Space))
                TryShoot();
        }
        else
        {
            // Modern control rotate
            float newAngle = -(Mathf.Atan2(joystick.Horizontal, joystick.Vertical)) * Mathf.Rad2Deg;
            if (newAngle != 0f) angle = newAngle;

            // Retro control thrust
            thrustVector = (new Vector2(joystick.Horizontal, joystick.Vertical) * (acceleration * GameManager.Instance.CurrentGameSpeed)) * Time.deltaTime;
        }

        // Apply movement
        rigidBody.AddForce(thrustVector);
        gameObject.transform.rotation = Quaternion.Euler(0, 0, angle);

        // Cap speed
        float currentSpeed = rigidBody.velocity.magnitude;
        if (currentSpeed > (maxSpeed * GameManager.Instance.CurrentGameSpeed))
            rigidBody.velocity = rigidBody.velocity.normalized * (maxSpeed * GameManager.Instance.CurrentGameSpeed);

        // Update effects
        if (thrustVector.magnitude > 0f)
        {
            spriteRenderer.sprite = thrustFrame;
            if (!thrustSound.isPlaying)
                thrustSound.Play();

            // Handle network effect syncing
            if (GameManager.Instance.NetManager.isNetworkActive)
            {
                if (GameManager.Instance.IsHost)
                    IsPlayerThrusting = true;
                else
                    CmdUpdateClientThrustState(true);
            }
        }
        else
        {
            thrustSound.Stop();
            spriteRenderer.sprite = staticFrame;

            // Handle network effect syncing
            if (GameManager.Instance.NetManager.isNetworkActive)
            {
                if (GameManager.Instance.IsHost)
                    IsPlayerThrusting = false;
                else
                    CmdUpdateClientThrustState(false);
            }
        }
    }

    // Called from shoot button
    public void TryShoot()
    {
        // If paused
        if (GameManager.Instance.IsPaused) return;

        // Solo game
        if (!GameManager.Instance.NetManager.isNetworkActive)
        {
            CreateProjectile();
        }
        else
        {
            // Disable shooting for non local player
            if (!isLocalPlayer) return;

            // Add host bullet to network
            if (isServer)
            {
                GameObject projectile = CreateProjectile();
                if (projectile != null)
                    NetworkServer.Spawn(projectile);
            }
            // Request host spawn client bullet
            else
            {
                CmdRequestCreateProjectile();
            }
        }
    }

    // Create a new projectile
    private GameObject CreateProjectile()
    {
        if (checkReloadedScript.CanShoot)
        {
            GameObject bullet = Instantiate(bulletPrefab, gunPosition.position, gunPosition.rotation);
            Rigidbody2D bulletRB = bullet.GetComponent<Rigidbody2D>();
            bulletRB.AddForce((Vector3)rigidBody.velocity + (gunPosition.up * (bulletForce * GameManager.Instance.CurrentGameSpeed)), ForceMode2D.Impulse);
            checkReloadedScript.Reload();

            return bullet;
        }
        return null;
    }

    // Executes on host only
    [Command]
    void CmdRequestCreateProjectile()
    {
        // Create a bullet on network
        GameObject projectile = CreateProjectile();
        if (projectile != null)
            NetworkServer.Spawn(projectile);
    }

    // Executes on host only
    [Command]
    void CmdUpdateClientThrustState(bool flag) => IsPlayerThrusting = flag;
}
