﻿using UnityEngine;

public class TrailRenderScript : MonoBehaviour
{
    // Static variables
    public static float TrailTimeInSeconds;

    // Inspector variables
    [SerializeField] private float trailTimeInSeconds;
    [SerializeField] public GameObject AttachedTrailRenderer;
    [SerializeField] public GameObject TrailRendererPrefab;

    // Init
    private void Start()
    {
        TrailTimeInSeconds = trailTimeInSeconds;
        TrailRenderer trailRenderer = AttachedTrailRenderer.GetComponent<TrailRenderer>();
        GameManager.Instance.TrailRenderers.Add(trailRenderer);
        trailRenderer.time = TrailTimeInSeconds * (1f / GameManager.Instance.CurrentGameSpeed);
    }
}
