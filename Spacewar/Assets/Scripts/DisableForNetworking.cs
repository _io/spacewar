﻿using UnityEngine;
using Mirror;

public class DisableForNetworking : NetworkBehaviour
{
    // Inspector variables
    [SerializeField] private Behaviour[] disabledBehavioursForNonLocalPlayer;

    // Init
    void Start()
    {
        if (!isLocalPlayer)
        {
            foreach (Behaviour behaviour in disabledBehavioursForNonLocalPlayer)
                behaviour.enabled = false;
        }
    }
}
