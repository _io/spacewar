﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class GameManager : MonoBehaviour
{
    // Singleton
    public static GameManager Instance;

    // Inspector variables
    [SerializeField] public float JoystickDeadZone;
    [SerializeField] public float[] GameSpeeds;
    [SerializeField] public int GameSpeedIndex = 2;
    [SerializeField] private GameObject cannotConnectObject;
    [SerializeField] private Transform spawnPoint01;
    [SerializeField] private Transform spawnPoint02;
    [SerializeField] private GameObject prefabPlayer;
    [SerializeField] private GameObject prefabNPC;
    [SerializeField] private GameObject prefabNetworkPlayer;
    [SerializeField] private GameObject mainMenu;
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject pauseButton;
    [SerializeField] private GameObject gameplayGUI;
    [SerializeField] private GameObject sunObject;
    [SerializeField] private Text audioToggleText;
    [SerializeField] private Text speedToggleText;
    [SerializeField] private Text controlsToggleText;
    [SerializeField] public Joystick Joystick;
    [SerializeField] public Button FireButton;
    [SerializeField] public NetworkManager NetManager;
    [SerializeField] public Mirror.Discovery.NetworkDiscovery NetworkDiscoveryScript;
    [SerializeField] public Mirror.Discovery.NetworkDiscoveryHUD NetworkDiscoveryHUD;
    [SerializeField] public bool webGLInput;

    // Public variables
    [System.NonSerialized] public bool IsHost = false;
    [System.NonSerialized] public float CurrentGameSpeed = 1f;
    [System.NonSerialized] public bool IsPaused = false;
    [System.NonSerialized] public bool RetroControlsEnabled = false;
    [System.NonSerialized] public List<Rigidbody2D> RigidBodies = new List<Rigidbody2D>();
    [System.NonSerialized] public List<TrailRenderer> TrailRenderers = new List<TrailRenderer>();
    [System.NonSerialized] public List<Rigidbody2D> PlayerRigidBodies = new List<Rigidbody2D>();

    // Private variables
    private bool isHostAvailable = false;
    private float timeWhenPaused = 0f;
    private bool isAudioEnabled = true;
    private string controlScheme = "CONTROLS : NEW";
    private List<Vector2> velocities = new List<Vector2>();
    private List<float> trailTimes = new List<float>();

    // Init
    private void Awake()
    {
        // Set static reference
        Instance = this;

        // Load audio settings
        isAudioEnabled = PlayerPrefs.GetInt("isAudioDisabled") == 1 ? false : true;
        if (!isAudioEnabled)
        {
            AudioListener.pause = true;
            audioToggleText.text = "AUDIO : OFF";
        }

        // Load speed settings
        int hasLoaded = PlayerPrefs.GetInt("hasLoaded");
        PlayerPrefs.SetInt("hasLoaded", 1);
        if (hasLoaded == 1) GameSpeedIndex = PlayerPrefs.GetInt("gameSpeedIndex");
        CurrentGameSpeed = GameSpeeds[GameSpeedIndex];
        speedToggleText.text = "SPEED : " + CurrentGameSpeed + "x";

        // Load control settings
        RetroControlsEnabled = PlayerPrefs.GetInt("areRetroControlsEnabled") == 1 ? true : false;
        if (RetroControlsEnabled)
        {
            controlScheme = "CONTROLS : RETRO";
            controlsToggleText.text = controlScheme;
        }
    }

    // End current game
    public void ReturnToMainMenu()
    {
        // Hide menus
        mainMenu.SetActive(true);
        pauseButton.SetActive(true);
        sunObject.SetActive(false);
        gameplayGUI.SetActive(false);
        pauseMenu.SetActive(false);
        CurrentGameSpeed = GameSpeeds[GameSpeedIndex];

        // Quit multiplayer match
        if (NetManager.isNetworkActive)
        {
            if (IsHost)
                if (NetworkClient.isConnected)
                    NetworkManager.singleton.StopHost();
            else
                NetworkManager.singleton.StopClient();
        }

        // Delete game objects
        foreach (Rigidbody2D rb in RigidBodies)
        {
            if (rb != null)
            {
                if (NetManager.isNetworkActive)
                    NetworkServer.Destroy(rb.gameObject);
                else
                    Destroy(rb.gameObject);
            }
        }

        // Delete trails
        foreach (TrailRenderer trail in TrailRenderers)
            if (trail != null)
                Destroy(trail.gameObject);

        // Clear lists
        RigidBodies.Clear();
        PlayerRigidBodies.Clear();
        TrailRenderers.Clear();
        velocities.Clear();
        trailTimes.Clear();

        // Reset states
        IsHost = false;
        IsPaused = false;
        isHostAvailable = false;
    }

    // Freeze all game objects
    public void PauseGame()
    {
        // Don't stop game if multiplayer
        if (NetManager.isNetworkActive) return;

        // Pause objects
        IsPaused = true;
        timeWhenPaused = Time.time;
        foreach (Rigidbody2D rb in RigidBodies)
        {
            if (rb == null) break;
            velocities.Add(rb.velocity);
            rb.velocity = Vector2.zero;
        }

        // Pause trails
        foreach (TrailRenderer tr in TrailRenderers)
        {
            if (tr == null) break;
            trailTimes.Add(tr.time);
            tr.time = 1000000;
        }
    }

    // Resume all game objects
    public void ResumeGame()
    {
        // Game didn't stop if multiplayer
        if (NetManager.isNetworkActive) return;

        // Resume objects
        IsPaused = false;
        int index = 0;
        foreach (Rigidbody2D rb in RigidBodies)
        {
            if (rb != null)
            {
                if (index < velocities.Count)
                {
                    rb.velocity = velocities[index];
                    index++;
                }
            }
        }
        velocities.Clear();

        // Resume trails
        index = 0;
        float timePaused = Time.time - timeWhenPaused;
        foreach (TrailRenderer tr in TrailRenderers)
        {
            if (tr != null)
            {
                if (index < trailTimes.Count)
                {
                    tr.time = trailTimes[index] + timePaused;
                    index++;
                }
            }
        }
        trailTimes.Clear();
    }

    // Start Solo
    public void StartSoloGame()
    {
        IsHost = false;
        if (NetworkDiscoveryScript != null)
            NetworkDiscoveryScript.StopDiscovery();
        GameObject player = Instantiate(prefabPlayer, spawnPoint01.position, Quaternion.Euler(0, 0, 0));
        GameObject computer = Instantiate(prefabNPC, spawnPoint02.position, Quaternion.Euler(0, 0, 0));
    }

    // Host LAN
    public void HostLANGame()
    {
        NetworkDiscoveryHUD.Host();
        IsHost = true;
    }

    // Join LAN
    public void JoinLANGame()
    {
        Debug.Log("Connected? " + isHostAvailable);
        if (isHostAvailable)
        {
            sunObject.SetActive(true);
            mainMenu.SetActive(false);
            gameplayGUI.SetActive(true);
            NetworkDiscoveryHUD.Join();
            isHostAvailable = false;
        }
        else
        {
            cannotConnectObject.SetActive(true);
            NetworkDiscoveryHUD.StartDiscovery();
        }
    }

    // Toggle audio on/off
    public void ToggleAudio()
    {
        if (isAudioEnabled)
        {
            isAudioEnabled = false;
            AudioListener.pause = true;
            audioToggleText.text = "AUDIO: OFF";
        }
        else
        {
            isAudioEnabled = true;
            AudioListener.pause = false;
            audioToggleText.text = "AUDIO: ON";
        }
        int audioDisabledFlag = isAudioEnabled ? 0 : 1;
        PlayerPrefs.SetInt("isAudioDisabled", audioDisabledFlag);
    }

    // Change game speed
    public void ToggleSpeed(bool isAutomaticUpdate)
    {
        // Disable changing speed for client
        if (NetManager.isNetworkActive && !IsHost && !isAutomaticUpdate) return;

        // Update game speed
        GameSpeedIndex++;
        if (GameSpeedIndex > GameSpeeds.Length - 1) GameSpeedIndex = 0;
        CurrentGameSpeed = GameSpeeds[GameSpeedIndex];
        speedToggleText.text = "SPEED : " + CurrentGameSpeed + "x";
        PlayerPrefs.SetInt("gameSpeedIndex", (int)GameSpeedIndex);
    }

    // Change game controls
    public void ToggleControls()
    {
        if (controlScheme == "CONTROLS : NEW")
        {
            RetroControlsEnabled = true;
            controlScheme = "CONTROLS : RETRO";
        }
        else
        {
            RetroControlsEnabled = false;
            controlScheme = "CONTROLS : NEW";
        }
        controlsToggleText.text = controlScheme;
        int retroEnabledFlag = RetroControlsEnabled ? 1 : 0;
        PlayerPrefs.SetInt("areRetroControlsEnabled", (int)retroEnabledFlag);
    }

    // Called from network discovery
    public void SetHostFound()
    {
        isHostAvailable = true;
        print("Host found");
    }
}