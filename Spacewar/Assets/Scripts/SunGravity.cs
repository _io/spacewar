﻿using UnityEngine;

public class SunGravity : MonoBehaviour
{
    // Load in rigidbodies
    [SerializeField] private Rigidbody2D rigidBody;

    // Editor controls
    [SerializeField] private float gravityScale = 1f;
    [SerializeField] private float rotationSpeed = 1f;

    // Update is called once per frame
    void Update()
    {
        // Game paused
        if (GameManager.Instance.IsPaused) { return; }

        // Attract ships
        foreach (Rigidbody2D playerRigidBody in GameManager.Instance.PlayerRigidBodies)
        {
            if (playerRigidBody != null)
            {
                Vector2 direction = rigidBody.position - playerRigidBody.position;
                float distance = direction.magnitude;
                float forceMagnitude = ((rigidBody.mass * playerRigidBody.mass) / Mathf.Pow(distance, 2)) * ((gravityScale * GameManager.Instance.CurrentGameSpeed) * Time.deltaTime);
                Vector2 force = direction.normalized * forceMagnitude;
                playerRigidBody.AddForce(force);
            }
        }

        // Rotate Sun
        rigidBody.transform.Rotate(new Vector3(0, 0, Random.Range(0, 360)));
    }
}
